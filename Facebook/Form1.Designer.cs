﻿namespace Facebook
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnLikes = new System.Windows.Forms.Button();
            this.lblQtd = new System.Windows.Forms.Label();
            this.img1 = new System.Windows.Forms.PictureBox();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lblGostos = new System.Windows.Forms.Label();
            this.lblNgostos = new System.Windows.Forms.Label();
            this.btnDislikes = new System.Windows.Forms.Button();
            this.lblData = new System.Windows.Forms.Label();
            this.tBTitulo = new System.Windows.Forms.TextBox();
            this.tBDesc = new System.Windows.Forms.TextBox();
            this.btnGravar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.img1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLikes
            // 
            this.btnLikes.BackColor = System.Drawing.SystemColors.Window;
            this.btnLikes.Image = ((System.Drawing.Image)(resources.GetObject("btnLikes.Image")));
            this.btnLikes.Location = new System.Drawing.Point(68, 305);
            this.btnLikes.Name = "btnLikes";
            this.btnLikes.Size = new System.Drawing.Size(43, 41);
            this.btnLikes.TabIndex = 3;
            this.btnLikes.UseVisualStyleBackColor = false;
            this.btnLikes.Click += new System.EventHandler(this.btnLikes_Click);
            // 
            // lblQtd
            // 
            this.lblQtd.BackColor = System.Drawing.SystemColors.Window;
            this.lblQtd.Location = new System.Drawing.Point(167, 319);
            this.lblQtd.Name = "lblQtd";
            this.lblQtd.Size = new System.Drawing.Size(31, 17);
            this.lblQtd.TabIndex = 4;
            // 
            // img1
            // 
            this.img1.Image = ((System.Drawing.Image)(resources.GetObject("img1.Image")));
            this.img1.Location = new System.Drawing.Point(43, 96);
            this.img1.Name = "img1";
            this.img1.Size = new System.Drawing.Size(463, 114);
            this.img1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img1.TabIndex = 5;
            this.img1.TabStop = false;
            // 
            // lbl2
            // 
            this.lbl2.BackColor = System.Drawing.SystemColors.Window;
            this.lbl2.Location = new System.Drawing.Point(430, 319);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(32, 17);
            this.lbl2.TabIndex = 8;
            // 
            // lblGostos
            // 
            this.lblGostos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblGostos.Location = new System.Drawing.Point(126, 317);
            this.lblGostos.Name = "lblGostos";
            this.lblGostos.Size = new System.Drawing.Size(35, 23);
            this.lblGostos.TabIndex = 9;
            this.lblGostos.Text = "Gosto";
            this.lblGostos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNgostos
            // 
            this.lblNgostos.Location = new System.Drawing.Point(366, 320);
            this.lblNgostos.Name = "lblNgostos";
            this.lblNgostos.Size = new System.Drawing.Size(58, 16);
            this.lblNgostos.TabIndex = 10;
            this.lblNgostos.Text = "Não Gosto";
            this.lblNgostos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btnDislikes
            // 
            this.btnDislikes.BackColor = System.Drawing.SystemColors.Window;
            this.btnDislikes.Image = ((System.Drawing.Image)(resources.GetObject("btnDislikes.Image")));
            this.btnDislikes.Location = new System.Drawing.Point(296, 305);
            this.btnDislikes.Name = "btnDislikes";
            this.btnDislikes.Size = new System.Drawing.Size(48, 41);
            this.btnDislikes.TabIndex = 11;
            this.btnDislikes.UseVisualStyleBackColor = false;
            this.btnDislikes.Click += new System.EventHandler(this.btnDislikes_Click);
            // 
            // lblData
            // 
            this.lblData.Location = new System.Drawing.Point(406, 12);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(100, 21);
            this.lblData.TabIndex = 13;
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tBTitulo
            // 
            this.tBTitulo.Location = new System.Drawing.Point(43, 12);
            this.tBTitulo.Multiline = true;
            this.tBTitulo.Name = "tBTitulo";
            this.tBTitulo.Size = new System.Drawing.Size(326, 70);
            this.tBTitulo.TabIndex = 14;
            // 
            // tBDesc
            // 
            this.tBDesc.Location = new System.Drawing.Point(43, 229);
            this.tBDesc.Multiline = true;
            this.tBDesc.Name = "tBDesc";
            this.tBDesc.Size = new System.Drawing.Size(463, 70);
            this.tBDesc.TabIndex = 15;
            // 
            // btnGravar
            // 
            this.btnGravar.Location = new System.Drawing.Point(409, 59);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(97, 23);
            this.btnGravar.TabIndex = 16;
            this.btnGravar.Text = "Publicar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(544, 367);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.tBDesc);
            this.Controls.Add(this.tBTitulo);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.btnDislikes);
            this.Controls.Add(this.lblNgostos);
            this.Controls.Add(this.lblGostos);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.img1);
            this.Controls.Add(this.lblQtd);
            this.Controls.Add(this.btnLikes);
            this.Name = "Form1";
            this.Text = "Facebook";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.img1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnLikes;
        private System.Windows.Forms.Label lblQtd;
        private System.Windows.Forms.PictureBox img1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lblGostos;
        private System.Windows.Forms.Label lblNgostos;
        private System.Windows.Forms.Button btnDislikes;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.TextBox tBTitulo;
        private System.Windows.Forms.TextBox tBDesc;
        private System.Windows.Forms.Button btnGravar;
    }
}

