﻿
namespace Facebook
{
    using System;
    public class Post
    {
        #region atributos

        private string titulo;
        private string descricao;
        private DateTime data;
        private int likes;
        private int dislikes;

        #endregion

        #region propriedades

        public string Titulo { get; set; }
        public string Descricao  { get; set; }
        public DateTime Data { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }

        #endregion

        #region métodos construtores

        public Post()
        {
            titulo = string.Empty;
            descricao = string.Empty;
            data = DateTime.MinValue;
            likes = 0;
            dislikes = 0;

        }//construtor default

        public Post ( DateTime data, bool like, bool dislike, int qtdlikes, int qtdislikes)
        {
            Titulo = titulo;
            Descricao = descricao;
            Data = data;
            Likes = likes;
            Dislikes = dislikes;

        }//construtor por parâmetros

        public Post(Post today)
        {
            this.Titulo = titulo;
            this.Descricao = descricao;
            this.Data = today.Data;
            this.Likes = today.Likes;
            this.Dislikes = today.Dislikes;
        }

        #endregion

        #region outros métodos

        public void Publicar()
        {
            data = DateTime.Now;
        }
        public int ContaLikes()
        {
            likes++;
            return likes;
        }
        public int ContaDislikes()
        {
            dislikes++;
            return dislikes;
        }
        #endregion
    }
}
