﻿namespace Facebook
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private Post bla;
        public Form1()
        {
            InitializeComponent();
            bla = new Post();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tBTitulo.Enabled = true;
            lblData.Text = DateTime.Now.ToString();
            tBDesc.Enabled = true;
            btnLikes.Enabled = false;
            lblQtd.Enabled = false;
            btnDislikes.Enabled = false;
            lbl2.Enabled = false;
        }

        private void btnLikes_Click(object sender, EventArgs e)
        {
            lblQtd.Text=bla.ContaLikes().ToString();
        }

        private void btnDislikes_Click(object sender, EventArgs e)
        {
            lbl2.Text = bla.ContaDislikes().ToString();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            bla.Titulo = tBTitulo.Text;
            bla.Descricao = tBTitulo.Text;
            tBDesc.Enabled = false;
            tBTitulo.Enabled = false;
            btnLikes.Enabled = true;
            lblQtd.Enabled = true;
            btnDislikes.Enabled = true;
            lbl2.Enabled = true;
        }
    }
}
